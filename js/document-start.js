﻿function init() {
  updateMode()

  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    updateMode()
  })
}

function updateMode() {
  chrome.storage.local.get(['mode'], result => {
    var mode = ''
    switch (result.mode) {
      case 0: mode = ''; break;
      case 1: mode = 'light'; break;
      case 2: mode = 'dark'; break;
    }

    document.getElementsByTagName('body')[0].className = mode
  })
}

init()
