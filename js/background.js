chrome.browserAction.onClicked.addListener(function() {
  chrome.storage.local.get(['mode'], function(result) {
    var mode = result.mode || 0;
    mode = (mode + 1) % 3;

    chrome.storage.local.set({ mode: mode });
    updateIcon(mode);

    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { event: 'modeChange' });
    });
  });
});

chrome.storage.local.get(['mode'], function(result) {
  updateIcon(result.mode || 0);
});


function updateIcon(mode) {
  var image = 'img/action_none.png';

  switch (mode) {
    case 0: image = 'img/action_none.png'; break;
    case 1: image = 'img/action_light.png'; break;
    case 2: image = 'img/action_dark.png'; break;
  }

  chrome.browserAction.setIcon({ path: image })
}


// Github stuff

const ISSUE_PATTERN = /^\/signalapp\/.*\/issues\/[0-9]+$/

chrome.webRequest.onCompleted.addListener(function(details) {
  const parsedUrl = new URL(details.url);

  if (parsedUrl.pathname.match(ISSUE_PATTERN)) {
    chrome.tabs.query({ active: true }, function(tabs) {
      tabs.forEach(function(tab) {
        chrome.tabs.sendMessage(tab.id, { type: 'page-rendered'});
      })
  });

  }
}, { urls: ['*://*.github.com/*'] });