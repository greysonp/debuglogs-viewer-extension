const DEBUGLOGS_PATTERN = /(https?:\/\/)?debuglogs\.org\/(?<id>[0-9a-f]+)/

function main() {
  replaceLinks()

  chrome.runtime.onMessage.addListener(function(request) {
    if (request && request.type === 'page-rendered') {
      setTimeout(500, replaceLinks())
    }
  });

  setInterval(replaceLinks, 3000)
}

function replaceLinks() {
  let links = document.querySelectorAll('a')

  for (let link of links) {
    let href = link.getAttribute('href')
    let result = DEBUGLOGS_PATTERN.exec(href)

    if (result) {
      link.setAttribute('href', `https://debuglogs.dev/${result.groups.id}`)

      if (DEBUGLOGS_PATTERN.exec(link.innerText)) {
        link.innerText = `https://debuglogs.dev/${result.groups.id}`
      }
    }
  }
}

main()