﻿const NBSP               = '&nbsp;'
const TITLE_PATTERN      = /^=+([a-zA-Z0-9\s]+)=+$/
const JOB_BODY_PATTERN   = /\[JOB::[a-f0-9\-]+\]\[[a-zA-Z0-9]+\](\[[0-9]\])?(.*)/ /* [jobId][name]([jobRunner])?(event) */
const JOB_TIMING_PATTERN = /\[(JOB::[a-f0-9\-]+)\]\[([a-zA-Z0-9]+)\]\[([0-9])\] Job finished with result SUCCESS in ([0-9]+) ms\. \(Time Since Submission: ([0-9]+) ms, Lifespan: (?:[0-9]+ ms|Immortal), Run Attempt: ([0-9]+)\// /* [(jobId)][(name)][(jobRunner)]?(runTime)(submitTime)(runAttempt) */
const UNDERSTATED        = '\u200a'

const Level = {
  V: 0,
  D: 1,
  I: 2,
  W: 3,
  E: 4,
  HEADING: 5,
  UNDERSTATED: 6,
  UNKNOWN: -1
}

function init() {
  let sections = getSections()

  writeHtml(sections)
  addExtraComponents(sections)
  improveTrace(sections)
  addListeners()
  showDefaults(sections)
}

function writeHtml(sections) {
  let html = ''

  sections.forEach(section => {
    html += `<div class="section-title closed" data-section-title="${section.title}">${section.title}</div>`
    html += `<div class="section hidden" data-section-title="${section.title}">`

    let linesHtml = getHtmlForLines(section.lines)
    html += linesHtml

    html += '</div>'
  })

  document.body.innerHTML = `<pre class="fancy">${html}</pre>`
}

function addExtraComponents(sections) {
  let loggerSection = getByTitle(sections, 'LOGGER')
  let element       = document.querySelector('.section[data-section-title="LOGGER"]')

  if (loggerSection != null && element != null) {
    let searchContainer = document.createElement('div')
    searchContainer.className = 'logger-search'

    let searchBox = document.createElement('input')
    searchBox.setAttribute('type', 'text')
    searchBox.setAttribute('placeholder', 'Filter...')
    searchBox.oninput = e => onLoggerSearchChanged(e.target.value, loggerSection)

    let clearButton = document.createElement('button')
    clearButton.innerText = 'Clear'
    clearButton.onclick = e => {
      searchBox.value = ''
      onLoggerSearchChanged('', loggerSection)
    }

    searchContainer.append(searchBox)
    searchContainer.append(clearButton)

    element.prepend(searchContainer)
  }
}

async function improveTrace(sections) {
  let traceSection = getByTitle(sections, 'TRACE')
  let element      = document.querySelector('.section[data-section-title="TRACE"]')

  if (traceSection != null && element != null) {
    let url = element.innerText.substring(0, element.innerText.indexOf(' ')) // the space in here is a special space
    element.innerHTML = `<a href="${url}">${url}</a><br /><br /><a href="https://ui.perfetto.dev/#!/" target="_blank">Perfetto Viewer</a><br /><br /><br />`
  }
}

function addListeners() {
  let titles = document.getElementsByClassName('section-title')

  for (let i = 0; i < titles.length; i++) {
    let title = titles[i]
    title.onclick = () => {
      toggleSection(title.dataset.sectionTitle)
    }
  }
}

function showDefaults(sections) {
  toggleSection('SYSINFO')
  toggleSection('LOGGER')

  let jobs = getByTitle(sections, 'JOBS')
  if (jobs != null && (jobs.lines.join('\n').match(/none/ig) || []).length !== 3) {
    toggleSection('JOBS')
  }

  let blocked = getByTitle(sections, 'BLOCKED THREADS')
  if (blocked != null && (blocked.lines.join('\n').match(/none/ig) || []).length !== 1) {
    toggleSection('BLOCKED THREADS')
  }

  let crashes = getByTitle(sections, 'CRASHES')
  if (crashes != null) {
    if (crashes.lines.length > 0) {
      toggleSection('CRASHES')
    }
    updateDisplayTitle('CRASHES', `CRASHES (${crashes.count})`)
  }
}

function getByTitle(sections, title) {
  for (let i = 0; i < sections.length; i++) {
    if (sections[i].title == title) {
      return sections[i]
    }
  }
  return null
}

function getSections() {
  let lines         = document.getElementsByTagName('pre')[0].innerHTML.split('\n')
  let sections      = []
  let activeSection = null

  lines.forEach(line => {
    if (line.length == 0) {
      line = NBSP
    }

    let titleMatch = line.match(TITLE_PATTERN)
    if (titleMatch != null) {
      if (activeSection != null) {
        sections.push(activeSection)
      }

      activeSection = {
        title: titleMatch[1].trim(),
        lines: []
      }
    } else if (activeSection != null) {
      activeSection.lines.push(line)
    }
  })

  if (activeSection != null) {
    sections.push(activeSection)
  }

  sections = addCustomSections(sections)

  return sections
}

function addCustomSections(sections) {
  let logger = getByTitle(sections, 'LOGGER')

  // CRASHES
  if (logger != null) {
    let exceptions = []
    let exception  = null

    logger.lines.forEach(line => {
      if (line.includes('SignalUncaughtExceptionHandler')) {
        exception = exception || []
        exception.push(line)
      } else if (exception != null) {
        exceptions.push(exception)
        exception = null
      }
    })

    if (exception != null) {
      exceptions.push(exception)
    }

    let section = {
      title: 'CRASHES',
      lines: exceptions.reduce((acc, val) => acc.concat(val).concat([NBSP, NBSP]), []),
      count: exceptions.length
    }

    sections.splice(sections.length - 2, 0, section)
  }

  // JOB EVENTS 
  if (logger != null) {
    let linesByJob = new Map()

    logger.lines.forEach(line => {
      let idIndex = line.indexOf('JOB::');

      if (idIndex > 0) {
        let id = line.substring(idIndex, idIndex + 41)

        if (!linesByJob.has(id)) {
          linesByJob.set(id, [])
        }

        linesByJob.get(id).push(line)
      }
    })

    let jobLines = []
    for (let [id, lines] of linesByJob) {
      let sampleLine = lines[0]

      let idTag     =`[${id}][`
      let start     = sampleLine.indexOf(idTag) + idTag.length
      let end       = sampleLine.indexOf(']', start)
      let className = sampleLine.substring(start, end)
      jobLines.push(`-- ${id} || ${className}`)

      for (const line of lines) {
        let time    = getTimeFromLine(line)
        let nameTag = `[${className}]`
        let match   = line.match(JOB_BODY_PATTERN)
        let body    = match && match.length >= 2 ? match[2] : ''

        jobLines.push(`${UNDERSTATED}${time} ${body}`)
      }

      jobLines.push(NBSP)
    }

    let section = {
      title: 'JOB EVENTS',
      lines: jobLines
    }

    sections.splice(2, 0, section)
  }

  // JOB METRICS
  if (logger != null) {
    let jobData = logger.lines.map(line => line.match(JOB_TIMING_PATTERN))
                              .filter(match => match != null)
                              .map(match => {
                                return {
                                  id:         match[1],
                                  name:       match[2],
                                  runner:     parseInt(match[3]),
                                  totalTime:  parseInt(match[5]),
                                  runTime:    parseInt(match[4]),
                                  queueTime:  parseInt(match[5]) - parseInt(match[4]),
                                  runAttempt: parseInt(match[6])
                                }
                              })
    
    let section =  {
      title: 'JOB METRICS',
      lines: buildJobMetricLines(jobData) 
    }

    sections.splice(3, 0, section)
  }

  // Localized SYSINFO time
  let sysInfo = getByTitle(sections, 'SYSINFO')
  if (sysInfo != null) {
    sysInfo.lines = sysInfo.lines.map(line => {
      if (line.startsWith('Time')) {
        let split = line.split(':')
        let time  = parseInt(split[1])
        let date  = new Date(time)
        return `${split[0]}: ${date.toLocaleString()} (${time})`
      } else {
        return line
      }
    })
  }

  return sections
}

function getLevelForLine(line, activeLevel) {
  if (line.startsWith('\t') || line.startsWith('Caused by')) {
    return activeLevel
  } else if (line.startsWith('-- ')) {
    return Level.HEADING
  } else if (line.startsWith(UNDERSTATED)) {
    return Level.UNDERSTATED
  } else if (line.indexOf(' E ') > 0) {
    return Level.E
  } else if (line.indexOf(' W ') > 0) {
    return Level.W
  } else if (line.indexOf(' I ') > 0) {
    return Level.I
  } else if (line.indexOf(' D ') > 0) {
    return Level.D
  } else if (line.indexOf(' V ') > 0) {
    return Level.V
  } else {
    return Level.UNKNOWN
  }
}

function getHtmlForLine(line, level) {
  switch (level) {
    case Level.E:
      return `<div class="log error"">${line}</div>`
    case Level.W:
      return `<div class="log warning">${line}</div>`
    case Level.I:
      return `<div class="log info">${line}</div>`
    case Level.D:
      return `<div class="log debug">${line}</div>`
    case Level.V:
      return `<div class="log verbose">${line}</div>`
    case Level.HEADING:
      return `<div class="log heading">${line}</div>`
    case Level.UNDERSTATED:
      return `<div class="log understated">${line}</div>`
    default:
      return `<div class="log unknown">${line}</div>`
  }
}

function getHtmlForLines(lines) {
  let html  = ''
  let level = null

  for (let i = 0; i < lines.length; i++) {
    let line = lines[i]
    level = getLevelForLine(line, level)
    line  = getHtmlForLine(line, level)

    html += line
  }

  return html
}

function toggleSection(name) {
  let title = document.querySelector(`.section-title[data-section-title="${name}"]`)
  let body  = document.querySelector(`.section[data-section-title="${name}"]`)

  if (title != null) {
    title.classList.toggle('closed')
  }

  if (body != null) {
    body.classList.toggle('hidden')
  }
}

function updateDisplayTitle(name, display) {
  let title = document.querySelector(`.section-title[data-section-title="${name}"]`)
  title.innerText = display
}

function getTimeFromLine(line) {
  let endIndex = Math.max.apply(Math, [line.indexOf(' V '),
                                       line.indexOf(' D '),
                                       line.indexOf(' I '),
                                       line.indexOf(' W '),
                                       line.indexOf(' E ')])
  return line.substring(0, endIndex)
}

function onLoggerSearchChanged(query, logger) {
  let filtered = logger.lines

  if (query && query.length > 0) {
    filtered = logger.lines.filter(l => l.toLowerCase().indexOf(query.toLowerCase()) >= 0)
  }

  document.querySelectorAll('.section[data-section-title="LOGGER"] .log').forEach(e => e.remove())

  let loggerElement = document.querySelector('.section[data-section-title="LOGGER"]')
  loggerElement.insertAdjacentHTML('beforeend', getHtmlForLines(filtered))
}

function buildJobMetricLines(data) {
  let lines  = []
  let byName = new Map()

  lines.push(buildSendJobSummary(data))
  lines.push(NBSP)

  data.sort((a, b) => a.name.localeCompare(b.name))
      .forEach(job => {
        if (!byName.has(job.name)) {
          byName.set(job.name, [])
        }

        byName.get(job.name).push(job)
      })

  for (let [name, jobs] of byName) {
    let metrics = computeJobMetrics(jobs)

    lines.push(`-- ${name}`)
    lines.push(`Count: ${jobs.length}`)
    lines.push(NBSP)
    lines.push(`Total Time`)
    lines = lines.concat(createJobMetricsLines(metrics.totalTime))
    lines.push(NBSP)
    lines.push(`Run Time`)
    lines = lines.concat(createJobMetricsLines(metrics.runTime))
    lines.push(NBSP)
    lines.push(`Queue Time`)
    lines = lines.concat(createJobMetricsLines(metrics.queueTime))
    lines.push(NBSP)
  }

  return lines
}

function createJobMetricsLines(metrics) {
  let lines = []
  lines.push(`  Average : ${metrics.average} ms`)
  lines.push(`  StdDev  : ${metrics.standardDeviation} ms`)
  lines.push(`  Outliers: ${metrics.outliers.length}`)

  metrics.outliers.forEach(outlier => {
    lines.push(`    ${outlier.id}`)
    lines.push(`      Total Time: ${outlier.totalTime} ms`)
    lines.push(`      Run Time  : ${outlier.runTime} ms`)
    lines.push(`      Queue Time: ${outlier.queueTime} ms`)
  })

  return lines
}

function computeJobMetrics(jobs) {
  const totalMetrics = computeJobMetricsForField(jobs, 'totalTime')
  const runMetrics   = computeJobMetricsForField(jobs, 'runTime')
  const queueMetrics = computeJobMetricsForField(jobs, 'queueTime')

  return {
    totalTime: totalMetrics,
    runTime:   runMetrics,
    queueTime: queueMetrics
  }
}

function computeJobMetricsForField(jobs, field) {
  let times             = jobs.map(job => job[field])
  let average           = computeAverage(times)
  let standardDeviation = computeStandardDeviation(times, average)
  let outliers          = jobs.filter(job => (job[field] - average) > standardDeviation)

  return {
    average:           Math.round(average),
    standardDeviation: Math.round(standardDeviation),
    outliers:          outliers
  }
}

function computeAverage(array) {
  return array.reduce((a, b) => a + b) / array.length
}

function computeStandardDeviation(array, mean) {
  return Math.sqrt(array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / array.length)
}

function buildSendJobSummary(data) {
  let byName = new Map()

  data.sort((a, b) => a.name.localeCompare(b.name))
      .forEach(job => {
        if (!byName.has(job.name)) {
          byName.set(job.name, [])
        }

        byName.get(job.name).push(job)
      })

  let interested = ['PushTextSendJob', 'PushMediaSendJob', 'PushGroupSendJob', 'SendDeliveryReceiptJob', 'SendReadReceiptJob',
                    'PushGroupSilentUpdateSendJob', 'ReactionSendJob', 'RemoteDeleteSendJob', 'SendViewedReceiptJob', 'ProfileKeySendJob']
  
  let output = ''
  let total  = 0

  for (let i = 0; i < interested.length; i++) {
    let jobs = byName.get(interested[i])
    if (jobs) {
      total += jobs.length
      output += interested[i] + ': ' + jobs.length + '\n';
    }
  }

  return 'Total: ' + total + '\n\n' + output
}

init()
